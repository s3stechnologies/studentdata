package com.practice.project.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import com.practice.project.model.ClassesEnrolled;

@Service
public class StudentJdbcServiceImpl {
	
	@Autowired
	JdbcTemplate template;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameter;
	
	
	
	//Post mapping
	public void saveClassEnrolled(ClassesEnrolled classEnrolled) {
		
		String sql = "INSERT INTO Class_Enrolled" 
					+ "(Class_Id, Class_Name, Semester)" + "VALUES(?,?,?)"	;
		Object[] objectArgs = {classEnrolled.getClassId(), classEnrolled.getClassName(), classEnrolled.getSemester()};
		
		List<Object[]> list = new ArrayList<>();
		list.add(objectArgs);
		template.update(sql,  objectArgs);
	}
	
	
	//Put
	public void insertClassEnrolled(ClassesEnrolled classEnrolled) {
		
		String sql = "UPDATE Class_Enrolled set Class_Name = :className, Semester = :semester where Class_Id = :classId";
		
		SqlParameterSource sourceParameter = new MapSqlParameterSource
				("classId", classEnrolled.getClassId()).addValue("className", classEnrolled.getClassName()).addValue("semester", classEnrolled.getSemester());
		
//		Object[] objectArgs = {classEnrolled.getClassId(), classEnrolled.getClassName(), classEnrolled.getSemester()};
//		
//		List<Object[]> list = new ArrayList<>();
//		list.add(objectArgs);
		
		namedParameter.update(sql, sourceParameter);
		
	}
	
	public String getClassNameById(int id) {
		String sql = "SELECT Class_Name FROM Class_Enrolled WHERE Class_Id = ?";
	
		String name = template.queryForObject(sql, new Object[] {id}, String.class);
		
		return name;
		
	}
	
	
	public ClassesEnrolled getClassById(int id) {
		String sql = "SELECT * from Class_Enrolled where Class_Id = ?";
		return (ClassesEnrolled) template.queryForObject(sql,new Object[] {id}, new BeanPropertyRowMapper(ClassesEnrolled.class));
		
	}
	
	
	
	
	
	
}





