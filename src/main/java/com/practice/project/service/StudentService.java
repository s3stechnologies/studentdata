package com.practice.project.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.practice.project.model.Student;
import com.practice.project.repo.StudentRepository;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	
	
	
	public List<Student> getAllStudents() {
		
		
		
		return studentRepository.findAll();
	}

	public void addStudent(List<Student> student) {
		studentRepository.saveAll(student);
		
	}

	public void updateStudent(Student student1, String id) {
		
//		for(Student student: studentList) {
//			if(student.getId().equals(id)) {
//				int i = studentList.indexOf(student);
//				studentList.set(i, student1);
//			}
//				
//		}
		
		studentRepository.save(student1);
	}

	public void deleteStudentsbyId(String id) {
//		for(Student student: studentList) {
//			if(student.getId().equals(id)) {
//				int i = studentList.indexOf(student);
//				studentList.remove(i);
//				return;
//			}
//				
//		}
		
		studentRepository.deleteById(id);;
	}
	
		
}
