package com.practice.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.project.model.Student;
import com.practice.project.service.StudentService;

@RestController
public class PracticeProjectController {

	@Autowired
	private StudentService studentService;
	
	
	@RequestMapping("/student")
	public List<Student> getApi() {
		return studentService.getAllStudents();
	}
	

	@PostMapping(value = "/student")
	//@RequestMapping(method = RequestMethod.POST, value = "/student")
	public void addStudent(@RequestBody List<Student> student) {
		studentService.addStudent(student);
	}
	
	@PutMapping(value = "/student/{id}")
	public void addStudent(@RequestBody Student student, @PathVariable String id) {
		studentService.updateStudent(student, id);
	}
	
	@DeleteMapping(value = "/student/{id}")
	public void deleteStudentByid(@PathVariable String id) {
		 studentService.deleteStudentsbyId(id);
	}
	
	
}
