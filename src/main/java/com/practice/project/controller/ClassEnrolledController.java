package com.practice.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.practice.project.model.ClassesEnrolled;
import com.practice.project.service.StudentJdbcServiceImpl;

@RestController
public class ClassEnrolledController {
	
	@Autowired
	 private StudentJdbcServiceImpl service;
	
	@PostMapping(value = "/class")
	public void saveClassEnrolled(@RequestBody ClassesEnrolled classEnrolled) {
		
		service.saveClassEnrolled(classEnrolled);
	}
	
	@PutMapping(value = "/class")
	public void updateClassEnrolled(@RequestBody ClassesEnrolled classEnrolled) {
		
		service.insertClassEnrolled(classEnrolled);
	}
	
	@GetMapping(value = "/class/{id}")
	public String getNameById(@PathVariable Integer id) {
		return service.getClassNameById(id) ;
		
	}
	
	@GetMapping(value = "/class/enrolled/{id}")
	public ClassesEnrolled getClassById(@PathVariable Integer id) {
		return service.getClassById(id) ;
		
	}
}
