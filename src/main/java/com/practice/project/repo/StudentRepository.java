package com.practice.project.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.practice.project.model.Student;

@Repository
@Transactional
//@FunctionalInterface
public interface StudentRepository extends JpaRepository<Student,String>{

	
	

}
