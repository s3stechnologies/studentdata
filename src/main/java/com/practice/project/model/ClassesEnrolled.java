package com.practice.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Class_Enrolled")
public class ClassesEnrolled {
	
	@Id
	@Column(name = "Class_Id", nullable = false)
	private Integer classId;
	
	@Column(name = "Class_Name")
	private String className;

	@Column(name = "Semester")
	private String Semester;
	
	public ClassesEnrolled() {
		
	}
	

	public ClassesEnrolled(Integer classId, String className, String semester) {
		super();
		this.classId = classId;
		this.className = className;
		Semester = semester;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getSemester() {
		return Semester;
	}

	public void setSemester(String semester) {
		Semester = semester;
	}
	
	
}
