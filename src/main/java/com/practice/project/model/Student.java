package com.practice.project.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student_table")
public class Student implements Serializable {
	
	private static final long serialVersionUID =1L;

	@Id
	@Column(name = "student_Id", nullable = false)
	private Integer studentId;
	
	@Column(name = "student_Name")
	private String studentName;
	
	@Column(name = "student_age")
	private Integer studentAge;
	
	public Student() {
		
	}
	
	public Student(int id, String name, int age) {
		this.studentId = id;
		this.studentName = name;
		this.studentAge = age;
		
	}
	
	

	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int id) {
		this.studentId = id;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String name) {
		this.studentName = name;
	}
	public Integer getStudentAge() {
		return studentAge;
	}
	public void setStudentAge(Integer age) {
		this.studentAge = age;
	}
	
}
